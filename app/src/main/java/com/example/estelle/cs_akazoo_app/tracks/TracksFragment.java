package com.example.estelle.cs_akazoo_app.tracks;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.estelle.cs_akazoo_app.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TracksFragment extends Fragment implements TracksView {

    RecyclerView mTracksRv;
    TrackPresenter presenter;


    public TracksFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_tracks, container, false);

        mTracksRv = v.findViewById(R.id.tracks_rv);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mTracksRv.setLayoutManager(layoutManager);

        presenter.getTracks();

        //CP FROM HERE FROM DATA 123
        return v;
    }

//    private void addMockTracks(ArrayList<Track> tracks) {
//        for (int i = 1; i < 100; i++) {
//            Track track = new Track("Track Name " + i, "Track Artist " + i, "TheCategory " + i);
//            tracks.add(track);
//        }
//    }


    @Override
    public void showTracks(ArrayList<Track> tracks) {
        //PASTE FROM HERE FROM DATA 123
        ArrayList<Track> tracks2 = new ArrayList<Track>();
//        addMockTracks(tracks);

        TracksRvAdapter tracksRvAdapter = new TracksRvAdapter(tracks);
        mTracksRv.setAdapter(tracksRvAdapter);
    }
}
