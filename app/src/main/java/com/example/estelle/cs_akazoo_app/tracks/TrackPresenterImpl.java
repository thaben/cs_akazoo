package com.example.estelle.cs_akazoo_app.tracks;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class TrackPresenterImpl implements TrackPresenter {

    TracksView view;

    public TrackPresenterImpl(TracksView view) {
        this.view = view;
    }

    @Override
    public void getTracks() {
        //edw prpei na kalesoume to call back
        view.showTracks(addMockTrack());
    }


    private ArrayList<Track> addMockTrack(){
        ArrayList<Track> tracks = new ArrayList<>();
        tracks.add(new Track("a","a","d"));
        tracks.add(new Track("a","a","d"));
        tracks.add(new Track("a","a","d"));
        tracks.add(new Track("a","a","d"));
        tracks.add(new Track("a","a","d"));

        return tracks;

    }
}
