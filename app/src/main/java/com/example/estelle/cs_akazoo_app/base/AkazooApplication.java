package com.example.estelle.cs_akazoo_app.base;

import android.app.Application;
import android.util.Log;

public class AkazooApplication extends Application {

    //private static AkazooApplication INSTANCE;

    public AkazooApplication() {

    }
    /*public static AkazooApplication getINSTANCE() {
        if (INSTANCE == null)
            INSTANCE = new AkazooApplication();
        return INSTANCE;
    }*/

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e("CS_TAG", "Application got created");
    }

    @Override
    public void onTerminate() {
        Log.e("CS_TAG", "Application got terinate");
        super.onTerminate();
    }
}
