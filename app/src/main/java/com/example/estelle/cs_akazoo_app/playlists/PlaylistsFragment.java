package com.example.estelle.cs_akazoo_app.playlists;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.estelle.cs_akazoo_app.R;
import com.example.estelle.cs_akazoo_app.tracks.TrackPresenter;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlaylistsFragment  extends Fragment implements PlaylistView {

    RecyclerView playlistsRv;
    PlaylistsPresenter presenter;

    public PlaylistsFragment() {
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_playlists, container, false);

        playlistsRv = v.findViewById(R.id.playslists_rv);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        playlistsRv.setLayoutManager(layoutManager);

        ArrayList<Playlist> playlists = new ArrayList<>();

        PlaylistsRvAdapter playlistsRvAdapter = new PlaylistsRvAdapter(playlists);
        playlistsRv.setAdapter(playlistsRvAdapter);
        presenter = new PlaylistsPresenterImpl(this);
        presenter.getPlaylists();

        return v;
    }




    @Override
    public void showPlaylists(ArrayList<Playlist> playlists) {

    }
}
